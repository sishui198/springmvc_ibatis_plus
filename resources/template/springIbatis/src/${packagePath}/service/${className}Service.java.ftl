package ${packName}.service;

import java.util.List;

import ${packName}.model.${className};

/**
 * 
 * ${className}Service接口
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public interface ${className}Service
{
    /**
     * 增加
     * 
     * @param ${className?uncap_first}
     * @see [类、类#方法、类#成员]
     */
    void insert(${className} ${className?uncap_first});
    
    /**
     * 根据id删除
     * 
     * @param id
     * @see [类、类#方法、类#成员]
     */
    void deleteById(Long id);
    
    /**
     * 根据id更新
     * 
     * @param ${className?uncap_first}
     * @see [类、类#方法、类#成员]
     */
    void updateById(${className} ${className?uncap_first});
    
    /**
     * 新增或更新
     * 
     * @param ${className?uncap_first}
     * @see [类、类#方法、类#成员]
     */
    void saveOrUpdate(${className} ${className?uncap_first});
    
    /**
     * 查询全部
     * 
     * @return
     * @see [类、类#方法、类#成员]
     */
    List<${className}> queryAll();
    
    /**
     * 根据id查询
     * 
     * @param id
     * @return
     * @see [类、类#方法、类#成员]
     */
    ${className} queryById(Long id);
    
}