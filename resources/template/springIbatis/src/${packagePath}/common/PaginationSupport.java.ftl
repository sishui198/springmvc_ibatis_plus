package ${packName}.common;

import java.util.LinkedList;
import java.util.List;

/**
 * 分页对象
 * 
 * @author 00fly
 * @version [版本号, ${date?date}]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class PaginationSupport
{
    
    private int currentPage; // 当前页
    
    private int total; // 总记录数
    
    private int pageSize; // 每页大小
    
    private int totalPage; // 总页数
    
    private int previous; // 上一页
    
    private int next; // 下一页
    
    private List<?> items;
    
    public PaginationSupport(int totalcount, int pageNo, int pageSize)
    {
        items = new LinkedList<Object>();
        this.currentPage = Math.max(1, pageNo);
        this.pageSize = Math.max(5, pageSize);
        setTotal(totalcount);
    }
    
    public int getCurrentPage()
    {
        return currentPage;
    }
    
    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }
    
    public int getTotal()
    {
        return total;
    }
    
    public void setTotal(int totalcount)
    {
        total = totalcount;
        if (total == 0)
        {
            totalPage = 1;
        }
        else
        {
            totalPage = 1 + (totalcount - 1) / pageSize;
        }
        if (currentPage < 1)
        {
            currentPage = 1;
        }
        else if (currentPage > totalPage)
        {
            currentPage = totalPage;
        }
        
        if (currentPage == 1)
        {
            previous = 1;
        }
        else
        {
            previous = currentPage - 1;
        }
        if (totalPage == currentPage)
        {
            next = currentPage;
        }
        else
        {
            next = currentPage + 1;
        }
    }
    
    public int getPageSize()
    {
        return pageSize;
    }
    
    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }
    
    public int getTotalPage()
    {
        return totalPage;
    }
    
    public void setTotalPage(int totalPage)
    {
        this.totalPage = totalPage;
    }
    
    public int getPrevious()
    {
        return previous;
    }
    
    public void setPrevious(int previous)
    {
        this.previous = previous;
    }
    
    public int getNext()
    {
        return next;
    }
    
    public void setNext(int next)
    {
        this.next = next;
    }
    
    public List<?> getItems()
    {
        return items;
    }
    
    public void setItems(List<?> items)
    {
        this.items = items;
    }
    
}
